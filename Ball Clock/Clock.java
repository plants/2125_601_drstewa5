import java.util.*;

public class Clock {  			//clock class, contains three stack to represent the rails of the clock and a deque to hold the balls as they await processing 

	
	public class Ball { 		 //inner class ball takes an int to use as an id 
	
	private int id;				//the int is used to mark the balls so that the state of the clock can be observed by the order of balls

	public Ball(int id) {		//ball constructor 
		
		this.id = id;}

	}// end class ball

	
	public class BallStack extends Stack {	 //a stack implementation that handles max sizes of stacks and some ball related methods
	   
	    private boolean full = false;
	    private int maxSize;
	    
	    public BallStack(int size) {
	        
	        super();
	        
	        this.maxSize = size;
	    }

	    public boolean full() {						//a getter for fullness
	       
			return this.full;
		}

		public void setFull() {

			this.full = true;
		}
		
	}												//end class BallStack

	public static int numBalls;						//holds an int for the number of balls going into the clock
	public static boolean inOrder = true;			//holds a boolean to state wether the balls are in their starting order

	public static BallStack min;		   				// (size 4)	stack of balls to represent minutes rail of the clock
	public static BallStack fiveMin;	   				// (size 11) stack of balls to represent five minute increments rail of the clock
	public static BallStack hours;	  					// (size 12) stack of balls to represent hour rail of the clock
	
	public static Deque<Ball> ballQ;				//uses a deque (subclass of queue with more void methods) to hold the balls before the go into the rails
	public static Ball[] initialState;				//an array of balls that will be used to represent the initial state
	public static Ball[] currentState;				//another array of balls that will be used to represent future states

public Clock(int numBalls){							//clock constructor takes an int for the number of balls in the clock
	
	this.numBalls = numBalls;						//initialize numBalls

	buildBalls(numBalls);							//builds the balls, places them in the deque in order, and sets the initial state
	
	min = new BallStack(4);								//initializes stacks and the deque
	fiveMin = new BallStack(11);
	hours = new BallStack(12);
	ballQ = new ArrayDeque<Ball>();

		
	}

	public int run() {								//run method calculates the number of days by selection statements

		int dayTracker = 0;							//daytracker actually tracks 12hr cycles not days, multiplied by two in main

		do {										//allows one run before evaluation
		
			if (min.isFull()) {						//if min is full we check if the next rail is full

				if (fiveMin.full()) {				//if five min is full we check to see if the next rail is full

					if (hours.full()) { 			//if hours is full we dump all of the balls back into the deque in the following order as performed by the physical clock

							Clock.QAll(min);					//queue all the balls left in min back into the deque in order
							Clock.QAll(fiveMin);				//then queue all the balls left in fiveMin back into the deque in order
							ballQ.addLast(ballQ.poll());		//then add the ball that couldnt fit into hours back to the end of the deque
							Clock.QAll(hours);					//then queue all the balls left in hours back into the deque in order
							dayTracker ++;


							}
								
						else {					//otherwise there is space in hours, so we check fullness	
						
							hours.push(ballQ.poll());			//if it isn't full, push onto it the first ball from the deque
							Clock.QAll(min);					//queue all the balls left in min back into the deque in order
							Clock.QAll(fiveMin);				//then queue all the balls left in fiveMin back into the deque in order

							}	
					
				else {
					
					fiveMin.push(ballQ.poll());			//if five is empty push onto it the first ball from the deque
					Clock.QAll(min);					//then queue all the balls left in min back into the deque in order
					

						} //end 5 min empty  (GOOD) min = full, fmin not empty


			else {					//otherwise there is at least 1 ball in min, so we check fullness
					
				min.push(ballQ.poll());				//if it isn't full, push onto it the first ball from the deque

				}				
			}
		}

		currentState = ballQ.toArray(new Ball[numBalls]);		//after our selection we set currentState 
		checkState();

		} while(inOrder = false);

	return dayTracker;
		
	}
		

	public void buildBalls(int numBalls){
		
		for(int i = 0; i <= numBalls; i++){

			Ball ball = new Ball(i);

			ballQ.addLast(ball);

			initialState = ballQ.toArray(new Ball[numBalls]);
		}
	}


	public static void QAll(Stack s){
		
		for ( int i = 0; i < s.size(); i++ ) {
			
			ballQ.addLast((Ball)s.pop());
		}
	}


	public static void checkState(){

		if (currentState == initialState){
			
			inOrder = true;
		}

	}
 	

 	public static void buildCurrentState(){//incomplete method that builds an array of ints representing each of the balls states in the order recieved
											//it would use this array to compare to the initial state array which was meant to be an array of balls 
											//i had to make an array of ints because otherwise i had no way of telling which ball was which	
		Deque<Ball> ballQTwo = ballQ;

		int[] ballIDs = new int[ballQ.size()] 

		for (int i = 0; i <= ballQ.size() ; i++ ) {

			ballIDs[i] = ballQtwo.pollFirst();

		for (int j ==0 ;i<ballIDs.length-1;i++ ) {
		
			buildBalls (ballIDs[j]);
		}
						
		}
	}

}