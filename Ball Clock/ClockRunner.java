import java.util.Scanner;

public class ClockRunner{
	
	public static Scanner sc = new Scanner(System.in);	//builds a scanner for input
	
	public static int days;								//holds an int for the amount of days it takes to cycle the balls
	
	public static int numBalls;							//holds an int for the amount of balls in the clock
	
	public static boolean valid = false;				//a boolean to maintain a system dialogue loop
	
	public static String prompt =("\nThis program takes in an amount of balls\n"+			//user dialogue and prompt
									"and runs them through a ball clock simulator.\n"+
									"The simulator will process the balls in such\n"+
									"a way that they will begin in order, eventually\n"+
									"become out of order, and in time will return to\n"+
									"their original order. This program determines\n"+
									"the approximate time in days until the balls\n"+
									"return to their original, in order state.\n\n"+
									
									"Do you have the balls? Enter how many balls you have,\n"+
									"it should be an integer from 27 - 127.\n");

	public static void main(String[] args) {
		
		System.out.println(prompt);							//displays the prompt

		numBalls = sc.nextInt();							//grabs an int for numballs

		while(valid == false) {								//loop system dialogue until appropriate input is entered
			
			if(numBalls < 27 || numBalls > 127)	{			//valid range check
			
				System.out.println("\ncheck your balls dog!\n(27-127 please)\n\n");		//input is out of range

				numBalls = sc.nextInt();
			
			}

			else { 

				Clock clock = new Clock(numBalls);
				
				days = (clock.run()*2);
				
				System.out.println("\nIt took "+days+" days to cycle "+numBalls+" balls.");	 
				
				valid = true;

			}
		}//end while loop
	}//end main
}//end class