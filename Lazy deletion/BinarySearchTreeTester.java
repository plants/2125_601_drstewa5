import static org.junit.Assert.*;
import org.junit.*;

public class BinarySearchTreeTester {

    // text fixture (the objects you are going to test go here as instance variables

    private BinarySearchTree<Integer> theBST;

    @Before
    public void setup() {
        //  build your test fixture objects here
        theBST = new BinarySearchTree<Integer>();
    }// end @Before method

    @Test
    public void testThatEmptyBSTIsEmpty() {
        // expected result , actual result
        assertEquals(true , theBST.isEmpty());
    }

    @Test
    public void testThatFullyDeletedBSTIsEmpty() {
        theBST.insert(20);
        theBST.insert(10);
        theBST.insert(30);
        theBST.remove(20);
        theBST.remove(10);
        theBST.remove(30);
        // expected result , actual result
        assertEquals(true , theBST.isEmpty());
	}

    @Test
    public void testThatClearedBSTIsEmpty() {
        theBST.insert(20);
        theBST.insert(10);
        theBST.insert(30);
        theBST.makeEmpty();
        // expected result , actual result
        assertEquals(true , theBST.isEmpty());
	}

    @Test
    public void testFindMin() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.insert(5);
        // expected result , actual result
        try {
            assertEquals( 5 , (int)(theBST.findMin()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMinOnEmptyBST() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.insert(5);
        theBST.makeEmpty();
        // expected result , actual result
        try {
            assertEquals( 5 , (int)(theBST.findMin()));
        } catch (UnderflowException e) {
            assertTrue(true);
        }
	}

    @Test
    public void testFindMinOnModifiedBST1() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(10);
        // expected result , actual result
        try {
            assertEquals( 5 , (int)(theBST.findMin()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMinOnModifiedBST2() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(5);
        // expected result , actual result
        try {
            assertEquals( 10 , (int)(theBST.findMin()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMinOnModifiedBST3() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(30);
        // expected result , actual result
        try {
            assertEquals( 5 , (int)(theBST.findMin()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMax() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        // expected result , actual result
        try {
            assertEquals( 100 , (int)(theBST.findMax()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMaxOnEmptyBST() {
        theBST.insert(30);
        theBST.insert(20);
        theBST.insert(10);
        theBST.insert(100);
        theBST.insert(40);
        theBST.insert(60);
        theBST.insert(5);
        theBST.makeEmpty();
        // expected result , actual result
        try {
            assertEquals( 100 , (int)(theBST.findMax()));
        } catch (UnderflowException e) {
            assertTrue(true);
        }
	}

    @Test
    public void testFindMaxOnModifiedBST1() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(60);
        // expected result , actual result
        try {
            assertEquals( 100 , (int)(theBST.findMax()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMaxOnModifiedBST2() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(100);
        // expected result , actual result
        try {
            assertEquals( 60 , (int)(theBST.findMax()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testFindMaxOnModifiedBST3() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(30);
        // expected result , actual result
        try {
            assertEquals(100 , (int)(theBST.findMax()));
        } catch (UnderflowException e) {
            e.printStackTrace();
        }
	}

    @Test
    public void testContainsOnExistingElement() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        // expected result , actual result
        assertTrue(theBST.contains(40));
	}

    @Test
    public void testContainsOnNonExistentElement() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        // expected result , actual result
        assertFalse(theBST.contains(110));
	}

    @Test
    public void testContainsOnRemovedElement() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(40);
        // expected result , actual result
        assertFalse(theBST.contains(40));
	}

    @Test
    public void testContainsOnRemovedRoot() {
        theBST.insert(30);
        theBST.insert(10);
        theBST.insert(5);
        theBST.insert(20);
        theBST.insert(60);
        theBST.insert(100);
        theBST.insert(40);
        theBST.remove(30);
        // expected result , actual result
        assertFalse(theBST.contains(30));
	}

	@After
	public void cleanup() {
        // happens after ever method labeled @Test
        // do cleanup here if necessary
	} // end @After method

} // end class JUnitExample
