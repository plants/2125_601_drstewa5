public class BinarySearchTree<AnyType extends Comparable<? super AnyType>>
	{

	private static class BinaryNode<AnyType> //an inner class that builds a node
		{
			BinaryNode( AnyType theElement )  //  basic constructor that takes in an element
				{ this( theElement, null, null ); }  // calls the other constructor and passes in two nulls alongside the element
		
			BinaryNode( AnyType theElement, BinaryNode<AnyType> lt, BinaryNode<AnyType> rt )  //  more defined constructor builds left and right
				{ element = theElement; left = lt; right = rt; }							  //  children, sets them as null

			boolean deleted = false;
			AnyType element; //the data in the node
			BinaryNode<AnyType> left; //left child
			BinaryNode<AnyType> right; //right child
		}

	private int nodes;
	private BinaryNode<AnyType> root; //sets a node in the tree apart as the root

	public BinarySearchTree() // constructor builds a tree starting with the root making it null
		{ root = null; }
	

	public void makeEmpty( ) // makes the root null so the tree is empty
		{ 
			if( root != null);
				makeEmpty( root );
		}

	private void makeEmpty( BinaryNode<AnyType> t )
		{		
			if(t != null)
			{
				if (t.deleted == false)
				{
					t.deleted = true;
					this.nodes --;
				}

				makeEmpty(t.left);
				makeEmpty(t.right);
			}	
		}

	public boolean isEmpty() // checks for null root
		{ 
			if(nodes == 0)
				return true; 
			
			else 
				return false;
		}

	
	public boolean contains( AnyType x ) // calls the contains helper method with an item
		{ return contains( x, root); }   // and passes it in with the item to find and the root which is where to start.
										 
	private boolean contains( AnyType x, BinaryNode<AnyType> t )  // contains helper method
		{
			if( t == null )  // if the root is null the tree is empty and it returns false bc it doesnt contain a thing
				return false;

			int compareResult = x.compareTo( t.element );  // compares the element (x) to the element in the root

			if( compareResult < 0 )  // if the item is considered less than whats in the root, it calls the method again and goes to the left child
				return contains( x, t.left );
			
			else if( compareResult > 0)   // if the item is considered more than whats in the root, it calls the method again and goes to the right child
				return contains( x, t.right );
			
			else if( compareResult == 0 && !t.deleted )  // if we find the element but the node is not marked as deleted we return true
				return true;
			
			else
				return false; // otherwise its not there so its false
		}


	public AnyType findMin () throws UnderflowException
		{ 	
 			if( isEmpty() ) throw new UnderflowException();
			return findMin( root, root ).element;  // 
		}

	private BinaryNode<AnyType> findMin( BinaryNode<AnyType> t, BinaryNode<AnyType> min )
		{
			if( t == null )
				return min;

			if( t.deleted == false )  //if its not deleted
			
			min = t;
			
			return findMin( t.left, min );
		}


	public AnyType findMax() throws UnderflowException
		{ 
			if( isEmpty() ) throw new UnderflowException();
			return findMax( root, root ).element;
		}

	private BinaryNode<AnyType> findMax( BinaryNode<AnyType> t, BinaryNode<AnyType> max )
		{
			if( t == null)
				return max;

			if( t.deleted == false )
			
			max = t;
			
			return findMax( t.right, max );
		}


	public void insert( AnyType x )
		{ 
			root = insert( x, root ); 
		}

	private BinaryNode<AnyType> insert( AnyType x, BinaryNode<AnyType> t )
		{
			if( t == null )

				{
					this.nodes ++;
					return new BinaryNode<>( x, null, null );
				}

			int compareResult = x.compareTo( t.element );

			if( compareResult < 0 )
				t.left = insert( x, t.left );
			
			else if( compareResult > 0 )
				t.right = insert( x, t.right );
			
			else if ( t.deleted == true )
				{
					unDeleter(t);
					this.nodes ++;
				}
			
			return t;
		}

		public void remove ( AnyType x )
			{ root = remove( x, root); } 

		private BinaryNode<AnyType> remove( AnyType x, BinaryNode<AnyType> t )
			{
				if( t == null )
					return t;  //  if the node is null, we return the null node that was passed in and nothing changes

				int compareResult = x.compareTo( t.element );  //  we take a comparison of the data of the element to
														 	   //  be removed and the data in the current root

				if( compareResult < 0 )  //  if the data is less than that of the data in the root move to the left
					t.left = remove( x, t.left );  
				
				else if( compareResult > 0 )  // if the data is more than that of the data in the node move to the right
					t.right = remove( x, t.right );

				else if (t.deleted == false)
					{
						deleter( t );  //  sets the node as "deleted"
						this.nodes --;
					}
				
				return t;
			}

	public void deleter( BinaryNode<AnyType> t )
			{ t.deleted = true; }
	
	public void unDeleter( BinaryNode<AnyType> t)
			{ t.deleted = false; }

	
	public void printTree()
		{
			if( isEmpty() )
				System.out.println( "Empty tree" );
			else
				printTree( root );
		}

	private void printTree( BinaryNode<AnyType> t)
		{
			if( t != null )
			{

				printTree( t.left);
				
				if(t.deleted = false) 
					System.out.println( t.element );
				
				printTree( t.right );
			}
		}
	private int height(BinaryNode<AnyType> t)
		{
			if(t== null)
				return -1;

		else
			{
				if(t.deleted = false)
					return 1 + Math.max(height(t.left), height (t.right));
				
				else 
					return Math.max(height(t.left), height(t.right));
			}
		}
	}